document.addEventListener('DOMContentLoaded', (event) => {const canvas = document.getElementById('snow');
const ctx = canvas.getContext('2d');

// Set canvas full screen
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

// Create an array to store snowflakes
let snowflakes = [];

// Snowflake class
class Snowflake {
    constructor() {
        this.x = Math.random() * canvas.width;
        this.y = Math.random() * canvas.height;
        this.size = Math.random() * 5 + 1;
        this.speed = Math.random() * 1 + 0.5;
    }

    // Method to update snowflake position
    update() {
        this.y += this.speed;
        if (this.y > canvas.height) {
            this.y = 0;
            this.x = Math.random() * canvas.width;
        }
    }

    // Method to draw snowflake
    draw() {
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.size, 0, Math.PI * 2);
        ctx.fillStyle = 'white';
        ctx.fill();
        ctx.closePath();
    }
}

// Create initial snowflakes
for (let i = 0; i < 100; i++) {
    snowflakes.push(new Snowflake());
}

// Animation loop
function animate() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    snowflakes.forEach(snowflake => {
        snowflake.update();
        snowflake.draw();
    });
    requestAnimationFrame(animate);
}

animate();

// Handle window resize
window.onresize = function() {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
};
});
