const colors = ["cyan", "orange", "blue", "yellow", "green", "purple", "red"];

const canvas = document.getElementById("game");
const ctx = canvas.getContext("2d");

const blockSize = 30;

const gridWidthPx = 10 * blockSize;

const gridX = (canvas.width - gridWidthPx) / 2;
const gridY = 100;

var interval;

oneFrameInputs = [];
inputs = [];

window.addEventListener("keydown", onKeyDown);
window.addEventListener("keyup", onKeyUp);

function onKeyDown(event)
{
    if(inputs.includes(event.code))
    {
        return;
    }

    inputs.push(event.code);
    oneFrameInputs.push(event.code);
}

function onKeyUp(event)
{
    let index = inputs.indexOf(event.code);
    if(index === -1)
    {
        return;
    }

    inputs.splice(index, 1);
}

function drawTile(x, y, color)
{
    ctx.beginPath();
    ctx.rect(x, y, blockSize, blockSize);
    ctx.fillStyle = color;
    ctx.fill();
    ctx.closePath();
}

function drawTetromino(x, y, matrix, color)
{
    for(let row = 0; row < matrix.size(); row++)
    {
        for(let col = 0; col < matrix.size(); col++)
        {
            if(matrix.get(row, col))
            {
                drawTile((x + col) * blockSize + gridX, (y + row) * blockSize + gridY, color);
            }

        }

    }

}

function getColorForTile(x, y, color1, color2)
{
    return y % 2 == x % 2 ? color1 : color2;
}

function drawGrid()
{
    for(let i = 0; i < gridWidth; i++)
    {
        for(let j = 0; j < gridHeight; j++)
        {
            let color = getColorForTile(i, j, "gray", "darkgray");
            if(grid[i][j] >= 0)
            {
                color = colors[grid[i][j]];
            }
            
            drawTile(gridX + i * blockSize, gridY + j * blockSize, color);
        }

    }

}

function drawNext(bag)
{
    for (let i = 0; i < 5; i++)
    {
        let piece = bag[i];
        let toDraw = blockRotations[piece][0];

        drawTetromino(11, (4 * i) + 1, toDraw, colors[piece]);
    }

}

function show()
{
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    drawGrid();
    let stop = update(inputs, oneFrameInputs);
    
    if(stop)
    {
        clearInterval(interval);
    }
    
    oneFrameInputs = [];
}

init();
interval = setInterval(show, framerate);
